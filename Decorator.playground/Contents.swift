import UIKit

protocol PC{
    var precio: Int { get }
    var descripcion: String { get }
}

class PCRegular : PC {

    var precio : Int {
        get {
            return 100
        }
    }
    var descripcion: String {
        get {
            return "PC de Escritorio"
        }
    }
}

class PCGamer : PC {
    var precio: Int {
        get {
            return 300
        }
    }
    var descripcion: String {
        get {
            return "PC Gamer"
        }
    }
}

class PCDecorator : PC {
    let instanciaPC : PC
    
    required init(pc: PC){
        self.instanciaPC = pc
    }
    var precio: Int {
        get {
            return instanciaPC.precio
        }
    }
    var descripcion: String {
        get {
            return instanciaPC.descripcion
        }
    }
}

final class MejoraDiscoDuro : PCDecorator {
    override var precio: Int {
        get {
            return instanciaPC.precio + 70
        }
    }

    override var descripcion: String {
        get {
            return instanciaPC.descripcion + ",SDD 1 TB"
        }
    }
    required init(pc: PC) {
        super.init(pc: pc)
    }
}
final class MejoraProcesador : PCDecorator {
    override var precio: Int {
        get {
            return instanciaPC.precio + 300
        }
    }
    override var descripcion: String {
        get {
            return instanciaPC.descripcion + ",Procesador i7 3.5 GHZ"
        }
    }
    required init(pc: PC) {
        super.init(pc: pc)
    }
}

final class MejoraRAM : PCDecorator {
    override var precio: Int {
        get {
            return instanciaPC.precio + 150
        }
    }
    override var descripcion: String {
        get {
            return instanciaPC.descripcion + ", RAM 2x8 GB"
        }
    }
    required init(pc: PC) {
        super.init(pc: pc)
    }
    
}

var pcNormal : PC = PCRegular()
print("PC Normal")
print("Precio : $\(pcNormal.precio), Descripcion: \(pcNormal.descripcion)")
pcNormal = MejoraRAM(pc: pcNormal)
print("\nPC Normal con mas RAM")
print("Precio : $\(pcNormal.precio), Descripcion: \(pcNormal.descripcion)")

var pcGamer : PC = PCGamer()
print("\nPC Gamer")
print("Precio : $\(pcGamer.precio), Descripcion: \(pcGamer.descripcion)")
pcGamer = MejoraRAM(pc: pcGamer)
pcGamer = MejoraProcesador(pc: pcGamer)
pcGamer = MejoraDiscoDuro(pc: pcGamer)
print("\nPC Gamer Mejorada")
print("Precio : $\(pcGamer.precio), Descripcion: \(pcGamer.descripcion)")
